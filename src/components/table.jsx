var ResponsiveTable = React.createClass({
  _head: function() {
    var columns = _.map(this.props.columns, function(colName) {
      return (
        <th>{colName}</th>
      );
    });
    return (
      <tr>{columns}</tr>
    );
  },
  
  _rows: function() {
    var _this = this;
    return _.map(_this.props.rows, function(row) {
      var values = _.map(_this.props.columns, function(colName, colKey) {
        return (
          <td data-label={colName}>{row[colKey]}</td>
        );
      })
      return (
        <tr>{values}</tr>
      );
    })
  },
  
  render: function() {
    return (
      <table className="responsive-table">
        <thead>
          {this._head()}
        </thead>
        <tbody>
          {this._rows()}
        </tbody>
      </table>
    );
  }
});
    
