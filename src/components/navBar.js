import React, { Component } from 'react';
import Button from 'react-bootstrap/lib/Button';

export default class NavBar extends Component {
  render() {
    return (
    <div>
      <div className="navmenu navmenu-inverse navmenu-fixed-left offcanvas-sm">
         <a className="navmenu-brand visible-md visible-lg" href="#">Asset Management</a>
         <ul className="nav navmenu-nav">
          <li>
            <a ui-sref="home">Assets
              <span className="pull-right glyphicon glyphicon-phone"></span></a>
            </li>
            <li>
              <a>User Guide
                <span className="pull-right 
                glyphicon glyphicon-book"></span></a>
              </li>
          </ul>   
        </div>

        <div className="navbar navbar-inverse navbar-fixed-top hidden-md hidden-lg">
          <button type="button" className="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <a className="navbar-brand" href="#">Asset Management</a>
        </div> 
    </div>
    );
  }
}



