import React, { Component } from 'react';
import NavBar from './components/navBar';

export default class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
    </div>
    );
  }
}
